<?php
/**
 * Template Name: Archive Page
 * The template for the archive page.
 *
 */
?>

<?php get_header(); ?>
<section class="blog blog animated fadeInUp">
<div class="container">  
<div class="row">
  <?php

    // set up our archive arguments
    $archive_args = array(
      post_type => 'post',    // get only posts
      'posts_per_page'=> -1   // this will display all posts on one page
    );

    // new instance of WP_Quert
    $archive_query = new WP_Query( $archive_args );

  ?>

    <?php $date_old = ""; // assign $date_old to nothing to start ?>
	
    <?php while ( $archive_query->have_posts() ) : $archive_query->the_post(); // run the custom loop ?>
		<div class="col-md-3 col-lg-3 col-xs-3 padding-none view view-second">
			<a href="<?php echo get_permalink(); ?>">	<?php 
					 if ( has_post_thumbnail() ) { 
					  the_post_thumbnail('archive', array('class' => 'img-responsive'));
					} 
				?>
                    <div class="mask"></div>
                    <div class="content2">
                        <h2><?php the_title(); ?></h2>
                    </div></a>
        </div>
		
     <?php $date_old = $date_new; // update $date_old ?>

    <?php endwhile; // end the custom loop ?>


  <?php wp_reset_postdata(); // always reset post data after a custom query ?>
</div>
</div>
</section>
<?php get_footer(); ?>