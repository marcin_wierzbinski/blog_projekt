 <?php get_header(); ?>
 <section class="blog">
    <div class="container">


      <div class="row"> 
        <div class="col-sm-10 col-lg-10 col-md-10 col-xs-12 blog-main">
           <div class="info row">
          <div class="col-lg-2 col-md-2 col-xs-5 text-left">
              <img src="<?php print IMG ?>/arrow-p.png" alt="home" />
          </div>   
          <div class="col-lg-7 col-md-7 col-xs-2 text-center">
              <img src="<?php print IMG ?>/home.png" alt="home" />
          </div>
          <div class="col-lg-2 col-md-2 col-xs-5 text-right">   
               <img src="<?php print IMG ?>/arrow-n.png" alt="home" />
          </div> 
         </div>
		 <?php if(have_posts()) : the_post(); ?> 
            <div class="blog-post">
         
            <div class="row">
                <div class="img-main">
                    <img class="img-responsive" src="<?php print IMG ?>/grafika.jpg" alt="grafika" /> 
                </div>
            <p class="blog-post-meta pull-left"><?php echo get_the_date(); ?></p>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12">
                    <div class="row text-left">
                        <h1><?php the_title(); ?></h1>
                    </div>
                    <div class="row cont">
						<?php echo the_content(); ?>
                    </div>  
					
                </div>
            </div>
            </div><!-- /.blog-post -->
			<?php endif; ?>
        </div><!-- /.blog-main -->

        <div class="col-sm-2 col-md-2 col-xs-12  col-lg-2">
              <?php get_sidebar('left'); ?>
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->
   </section>
  <?php get_footer(); ?>