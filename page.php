 <?php get_header(); ?>
 <section class="blog animated fadeInUp" style="position:relative;z-index:1;">
    <div class="container">
      <div class="row"> 
        <div class="col-sm-10 col-lg-10 col-md-10 col-xs-12 blog-main">
		 <?php if(have_posts()) : the_post(); ?> 
            <div class="blog-post">
         
            <div>
                <div class="img-main">
                    <?php 
					if ( has_post_thumbnail() ) {
					the_post_thumbnail('page', array('class' => 'img-responsive'));
					}
					?>
                </div>
            <!--<p class="blog-post-meta pull-left"><?php // echo get_the_date(); ?></p>-->
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12">
                    <div class=" text-left">
                        <h1  style="padding-top:10px;"><?php the_title(); ?></h1>
                    </div>
                    <div class="cont">
						<?php echo the_content(); ?>
                    </div>  
					
                </div>
            </div>
            </div><!-- /.blog-post -->
			<?php endif; ?>
        </div><!-- /.blog-main -->

        <div class="hidden-phone col-sm-2 col-md-2 col-xs-12  col-lg-2">
              <?php get_sidebar('right'); ?>
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->
   </section>
  <?php get_footer(); ?>