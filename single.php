 <?php get_header(); ?>
 <section class="blog animated fadeInUp">
    <div class="container">
      <div class="row"> 
        <div class="col-sm-10 col-lg-10 col-md-10 col-xs-12 blog-main">
           <div class="info">
		  <?php
			$pagelist = get_posts('sort_column=orderby&sort_order=asc');
			$pages = array();
			foreach ($pagelist as $page) {
			   $pages[] += $page->ID;
			}
			$current = array_search(get_the_ID(), $pages);
			$prevID = $pages[$current-1];
			$nextID = $pages[$current+1];
			?>
			<div class="col-lg-3 col-md-3 col-xs-3 text-left">
				<?php if (!empty($prevID)) { ?>
				
				<a class="line-height" href="<?php echo get_permalink($prevID); ?>"
				  title="<?php echo get_the_title($prevID); ?>">
				  <img class="other" src="<?php print IMG ?>/arrow-p.png" alt="home" />
					<div class="hidden-phone">Wstecz</div>
				</a>
				<?php } ?>
			</div>
			<div class="col-lg-6 col-md-6 col-xs-6 text-center">
				<a href="<?php echo home_url(); ?>"><img src="<?php print IMG ?>/home.png" alt="home" /></a>
			</div>
			<div class="col-lg-3 col-md-3 col-xs-3 text-right"> 
				<?php 
				if (!empty($nextID)) { ?>
					<a class="line-height" href="<?php echo get_permalink($nextID); ?>" 
					 title="<?php echo get_the_title($nextID); ?>">
					  <img class="pull-right" src="<?php print IMG ?>/arrow-n.png" alt="Arrow" /> 
					 <div class="hidden-phone">Nast&#281;pny</div>
					</a>
				<?php } ?>
			</div>
			<div class="divider-20"></div>
         </div>
		 
		 <?php if(have_posts()) : the_post(); ?> 
            <div class="blog-post">
         
            <div>
                <div class="img-main">
                   <?php 
				 if ( has_post_thumbnail() ) {
				  the_post_thumbnail('page', array('class' => 'img-responsive'));
					} 
					?>
                </div>
				<div class="headerBlog">
					<div style="padding-left:0px;" class="col-md-6"><p class="blog-post-meta"><?php echo get_the_date(); ?></p></div>
					<div class="pull-right col-md-6" style="line-height: 60px;"><?php if(function_exists('kc_add_social_share')) kc_add_social_share(); ?></div>
				</div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12">
                    <div class="text-left">
                        <h1 class="special"><?php the_title(); ?></h1>
                    </div>
                    <div class="cont">
						<?php echo the_content(); ?>
                    </div>  
                </div>
            </div>
			
            </div><!-- /.blog-post -->
			<?php endif; ?>
        </div><!-- /.blog-main -->

			<div class="hidden-phone col-sm-2 col-md-2 col-xs-12  col-lg-2">
                <?php get_sidebar('right'); ?>
              </div>
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->
   </section>
  <?php get_footer(); ?>