<?php 
define('THEMEROOT', get_stylesheet_directory_uri());
define('IMG', THEMEROOT . '/img');

add_filter('widget_text', 'do_shortcode');
add_filter( 'the_excerpt', 'do_shortcode');

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Menu Top' ),
    )
  );
}
add_action( 'init', 'register_my_menus' );

add_theme_support( 'post-thumbnails' );

add_image_size( 'page', 990, 240, true );
add_image_size( 'mina', 220, 220, true );
add_image_size( 'archive', 300, 200, true );

add_image_size( 'mina_rwd', 680, 220, true );


function blog_scripts() {
	/* Global Style*/
	wp_enqueue_style( 'blog-bootstrap', get_template_directory_uri() . '/css/bootstrap.css' );
	wp_enqueue_style( 'header-style', get_template_directory_uri() . '/css/style_header.css' );
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css' );
	
	if(is_page_template('archives.php')) {
		wp_enqueue_style( 'archives_style2', get_template_directory_uri() . '/archives/style2.css' );
		wp_enqueue_style( 'archives_style_common', get_template_directory_uri() . '/archives/style_common.css' );
	}
	
    wp_enqueue_script('emulation-js',get_stylesheet_directory_uri() . '/js/ie-emulation-modes-warning.js"',array( 'jquery' ), '1.0.0', false);
	wp_enqueue_script('emulation-10-js',get_stylesheet_directory_uri() . '/js/ie10-viewport-bug-workaround.js"',array( 'jquery' ), '1.0.0', false);
    wp_enqueue_script('bootstrap-js',get_stylesheet_directory_uri() . '/js/bootstrap.min.js"',array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('bootstrap-docs',get_stylesheet_directory_uri() . '/js/docs.min.js"',array( 'bootstrap-js' ), '1.0.0', true);
	/* End Global Style */
	
}
add_action( 'wp_enqueue_scripts', 'blog_scripts' );

function blog_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Left Sidebar', 'blog_edu' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Left Sidebar.', 'blog_edu' ),
		'before_widget' => '<aside id="%1$s" class="row widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Sidebar 1', 'blog_edu' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Right Sidebar', 'blog_edu' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Sidebar 2', 'blog_edu' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears in the footer section of the site.', 'blog_edu' ),
		'before_widget' => '<aside id="%1$s" style="margin-top:18px; margin-bottom:18px;"class="widget">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'blog_widgets_init' );

add_action('admin_menu', 'blog_edu_salon');
function blog_edu_salon() {

	add_menu_page('Ustawienia Edutainment', 'Ustawienia Edutainment', 'administrator', __FILE__, 'settings_blog_page');

	add_action( 'admin_init', 'register_mysettings' );
}
function register_mysettings() {
	register_setting( 'settings-group', 'facebook' );
	register_setting( 'settings-group', 'twitter' );
	register_setting( 'settings-group', 'google' );
	register_setting( 'settings-group', 'evernote' );
	//register_setting( 'settings-group', 'rss' );
	register_setting( 'settings-group', 'linkedin' );
	register_setting( 'settings-group', 'urlikon' );
	register_setting( 'settings-group', 'urlikonog' );
	register_setting( 'settings-group', 'descript' );
}
function settings_blog_page() {
?>
<div class="wrap">
<h2>Blog Edutainment.com.pl</h2>
<form method="post" action="options.php">
    <?php settings_fields( 'settings-group' ); ?>
    <?php do_settings_sections( 'settings-group' ); ?>
    <table class="form-table">
	<p> Pozostawienie pustego Url(nie wyswietla ikonki)</p>
        <tr valign="top">
			<th scope="row">Facebook</th>
				<td><input style="min-width:450px;" type="text" name="facebook" value="<?php echo get_option('facebook'); ?>" /></td>
        </tr>
		<tr valign="top">
			<th scope="row">Twitter</th>
				<td><input style="min-width:450px;" type="text" name="twitter" value="<?php echo get_option('twitter'); ?>" /></td>
        </tr>
		<tr valign="top">
			<th scope="row">Google</th>
				<td><input style="min-width:450px;" type="text" name="google" value="<?php echo get_option('google'); ?>" /></td>
        </tr>
		<tr valign="top">
			<th scope="row">Evernote</th>
				<td><input style="min-width:450px;" type="text" name="evernote" value="<?php echo get_option('evernote'); ?>" /></td>
        </tr>
		<tr valign="top">
		<!-- Powiazany -->
			<th scope="row">linkedin</th>
				<td><input style="min-width:450px;" type="text" name="linkedin" value="<?php echo get_option('linkedin'); ?>" /></td>
        </tr>
		<!--<tr valign="top">
			<th scope="row">RSS</th>
				<td><input style="min-width:450px;" type="text" name="rss" value="<?php //secho get_option('rss'); ?>" /></td>
        </tr>-->
		<h3>Url ikonki  </h3>
		<tr valign="top">
			<th scope="row">Url Ikonki</th>
				<td><input style="min-width:450px;" type="text" name="urlikon" value="<?php echo get_option('urlikon'); ?>" /></td>
        </tr>
		<tr valign="top">
			<th scope="row">Url Ikonki(OG MEDIA)</th>
				<td><input style="min-width:450px;" type="text" name="urlikonog" value="<?php echo get_option('urlikonog'); ?>" /></td>
        </tr>
		<tr valign="top">
		<th scope="row">Description</th>
			<td><textarea style="resize:none;" id="intro_text" name="descript" rows="5" cols="30"><?php echo get_option('descript'); ?></textarea>
			<br />w przypadku zmiany tego pola należy swojego bloga debugować :
			<a href="https://developers.facebook.com/tools/debug">tutaj </a>
			</td>
		</tr>
    </table>
    <?php submit_button(); ?>
</form>
</div>
<?php } 


function numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="navigation2"><ul class="list-unstyled">' . "\n";

	/**	Previous Post Link */
	//if ( get_previous_posts_link() )
	//	printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active pagination-li"' : '';

		printf( '<li%s class="pull-left  pagination-li"><a href="%s"></a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>�</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s class="pull-left  pagination-li"><a href="%s"></a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>�</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s class="pull-left  pagination-li"><a href="%s"></a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	//if ( get_next_posts_link() )
	//	printf( '<li>%s</li>' . "\n", get_next_posts_link() );

	echo '</ul></div>' . "\n";

}

  require get_template_directory() . '/include/custom-header.php';
  require get_template_directory() . '/include/shortcode.php';
?>
