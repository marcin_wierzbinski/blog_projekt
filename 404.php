 <?php get_header(); ?>
 <section class="blog animated fadeInUp">
    <div class="container">


      <div class="row"> 
        <div class="col-sm-10 col-lg-10 col-md-10 col-xs-12 blog-main">
		<p class="text-center"> 404 / STRONY NIE ZNALEZIONO</p>
		 <h3 class="text-center">Przepraszamy, strona o podanym adresie nie zosta&#322;a odnaleziona.</h3>
		 <div class="text-center""><a href="<?php echo home_url(); ?>">Home</a></div>
        </div><!-- /.blog-main -->

      </div><!-- /.row -->

    </div><!-- /.container -->
   </section>
  <?php get_footer(); ?>