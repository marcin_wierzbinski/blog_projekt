		<div class="blog-post">
			<div>
				<p class="blog-post-meta pull-left"><?php echo get_the_date(); ?></p>
			</div>
			<div class="clearfix"></div>
		<div class="row">
			<div class="col-sm-4 hidden-desktop col-md-4 col-xs-12 ">
				 <?php 
					 if ( has_post_thumbnail() ) { 
					  the_post_thumbnail('mina_rwd', array('class' => 'img-responsive pull-right'));
					} 
				?>  
			</div>
			<div class="col-sm-8 col-md-8 col-xs-12">
				<div class="text-left">
					<h1 class="specialfadein"><?php the_title(); ?></h1>
				</div>
				<div class="cont">
					<div><?php the_content(''); ?></div>
				</div>  
				<?php echo '<a class="pull-left special_left" href="'. get_permalink($post->ID) . '">' . 'wi&#281;cej' . '</a>'; ?>
			</div>
			<div class="col-sm-4 hidden-phone col-md-4 col-xs-12 ">
				 <?php 
					 if ( has_post_thumbnail() ) { 
					  the_post_thumbnail('mina', array('class' => 'img-responsive pull-right'));
					} 
				?>  
			</div>
		</div>
		</div><!-- /.blog-post -->	