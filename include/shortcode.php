<?php 

if (!function_exists('author_blok')) {
function author_blok($atts, $content = null) {
extract(shortcode_atts(array( "img_src" => "", "title" => ""), $atts));
    return '
	<div class="headers text-center">
	<img class="img-responsive" src="'. $img_src .'" alt="author" /> 
	<h3>'. $title .'</h3>
	<p>' . do_shortcode($content) . '</p>
	</div>';
}
}
add_shortcode('author_blok', 'author_blok');


