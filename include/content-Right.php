		<div class="blog-post">
			<div>
				<p class="blog-post-meta pull-right"><?php echo get_the_date(); ?></p>
			</div>
			<div class="clearfix"></div>
			<div class="row">
			<div class="col-sm-4 hidden-desktop col-md-4 col-xs-12 ">
				<?php 
					 if ( has_post_thumbnail() ) { 
					  the_post_thumbnail('mina_rwd', array('class' => 'img-responsive pull-right'));
					} 
				?>  
			</div>
				<div class="col-sm-4 hidden-phone col-md-4 col-xs-12 ">
					<?php 
						 if ( has_post_thumbnail() ) { 
						  the_post_thumbnail('mina', array('class' => 'img-responsive pull-left'));
						} 
					?>      
				</div>
				<div class="col-sm-8 col-md-8 col-xs-12 text-left">
					<h1 class="text-right specialfadein"><?php the_title(); ?></h1>
					<div class="clearfix"></div>
					<div class="text-right cont">
						<div><?php the_content(''); ?></div>
					</div>
					<?php echo '<a class="pull-right special_right" href="'. get_permalink($post->ID) . '">' . 'wi&#281;cej' . '</a>'; ?>
				</div>
			</div>
		</div><!-- /.blog-post -->