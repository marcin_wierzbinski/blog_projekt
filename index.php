 <?php get_header(); ?>
<section class="blog animated fadeInUp">
    <div class="container">


				<div class="row">

						<div class="col-sm-10 col-lg-10 col-md-10 col-xs-12 blog-main">
									<?php if ( have_posts() ) :
										$postcount = 1;
										while ( have_posts() ) : the_post() ;
										if( ($postcount % 2) == 0 ):
										?>
										<?php get_template_part( 'include/content', 'Left' ); ?>
										<?php else: ?>
												<!-- else -->
										<?php get_template_part( 'include/content', 'Right' ); ?>
									<?php
										endif;
										$postcount++;
										endwhile;
										endif;
									?>
								<div style="margin:0px auto;text-align:center;">
									<?php numeric_posts_nav(); ?>
								</div>
					
						</div><!-- /.blog-main -->

					<div class="testt col-sm-2 col-md-2 col-xs-12  hidden-phone col-lg-2">
						<?php get_sidebar( 'right' ); ?>
					</div><!-- /.blog-sidebar -->

				</div><!-- /.row -->

    </div><!-- /.container -->
   </section>
  <?php get_footer(); ?>