<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo get_option('descript'); ?>">
	<meta property="og:description" content="<?php echo get_option('descript'); ?>" />
    <meta name="author" content="">
    <link rel="icon" href="<?php echo get_option('urlikon'); ?>">
	<meta property="og:image" content="<?php echo get_option('urlikonog'); ?>">
     <title><?php bloginfo('name') ?> <?php wp_title('|', true, 'left') ?></title>
     <?php wp_head(); ?>
     
    <!-- Custom styles for this template -->
    <link href="<?php bloginfo('stylesheet_url') ?>" rel="stylesheet"/>
   
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
  </head>

  <body>
    <section class="slideDown fadeInDown animated headerMain" role="banner">
      <div class="container">
		  <div class="row">
			<div class="width-84">
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<?php if ( get_header_image() ) : ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<div class="logo"><img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt=""></div>
						</a>
					<?php endif; ?>
				</div>
				<div class="navbar-collapse collapse">
						<?php
						$defaults = array(
							'theme_location'  => 'header-menu',
							'menu_class'      => 'top-bar',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'items_wrap'      => '<ul class="nav navbar-nav">%3$s</ul>',
							'depth'           => 0,
						);
						wp_nav_menu( $defaults );
					?>
				</div><!--/.nav-collapse -->
			</div>
		</div>
      </div>
   </section>