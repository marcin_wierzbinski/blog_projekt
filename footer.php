	<section class="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-lg-10 col-md-10 col-xs-12 ">
					<div class="col-md-6 col-xs-6">
						<?php dynamic_sidebar( 'sidebar-2' ); ?>
					</div>
					<div class="col-md-6 col-xs-6 text-right">
						
						<?php dynamic_sidebar( 'sidebar-3' ); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <?php wp_footer(); ?>

		<script>
		   $(function() {
				$(window).scroll(function() {
				 clearTimeout($.data(this, 'scrollTimer'));
		   $.data(this, 'scrollTimer', setTimeout(function() {
			   var item = Math.floor(($(window).scrollTop()+$(window).height()-140)/371)-1,
					 position = (item<0)?371:item*371;
					 $(".testt").stop().animate({
							 marginTop: position
					 },1000);
		   }, 250));
				});
			});
	</script>
	
  </body>
</html>
